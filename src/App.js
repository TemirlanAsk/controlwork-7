import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Hamburger from "./components/Item/Item";
import Cheeseburger from "./components/Cheeseburger/Cheeseburger";
import Item from "./components/Item/Item"
import Food from "./components/Food/Food";

class App extends Component {
        state = {
            food: {
                Hamburger: {text:'Price: 80 KGS', amount: 0},
                Cheeseburger: {text:'Price: 90 KGS', amount: 0},
                Fries: {text:'Price: 45 KGS', amount: 0},
                Coffee: {text: 'Price 70 KGS', amount: 0},
                Tea: {text:'Price 50 KGS', amount: 0},
                Cola: {text:'Price 40 KGS', amount: 0}
            },
            Hamburger: ['Price: 80 KGS'],
            Cheeseburger: ['Price: 90 KGS'],
            Fries: ['Price: 45 KGS'],
            Coffee: ['Price 70 KGS'],
            Tea: ['Price 50 KGS'],
            Cola: ['Price 40 KGS']

        };

        addOrder = (add) => {
            console.log(add);
            let food = {...this.state.food};
            let order = {...food[add]};
            // order.push('s');
            order.amount++;
            food[add] = order;
            console.log(order);
            this.setState({
                food
            });

        };
        array = [];
            moveItem = (name) => {
                for (let i = 0; i < this.state.food[name].amount; i++) {
                    this.array.push(<Food/>)
                }
            };
        // deleteOrder = (less) => {
        //         let order = [...this.state[less]];
        //         order.pop();
        //         this.setState({
        //             [less]: order
        //         });
        //     };

    render() {

        return (
            <div className="Eat">
            <div className="AddItems">
                {Object.keys(this.state.food).map((item, index) => <Item item={item} price={item.price} name={item.text} key={index}  click={this.addOrder}/>)}
            </div>
                <div className="Order">
                    <p>Order details</p>
                    {this.array}
                </div>
            </div>
        )
    }
}

export default App;
