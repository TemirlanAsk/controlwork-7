import React from 'react';
import './Cheeseburger.css';

const  Cheeseburger = props => {
        return(
            <div className="Cheeseburger" >
               <button className="btn" onClick={() => props.click('Cheeseburger')}>
                   <h3>Cheeseburger</h3>
                   <p>{props.prices}</p>
               </button>
            </div>
        )
};
export default Cheeseburger;
