import React from 'react';
import './Item.css';


const Item = props => {
    return(
        <div className = {props.item} onClick={() => props.click(props.name)}>
            <button className="btn">
                <h3>{props.item}</h3>
                <p>{props.price}</p>
            </button>

        </div>
    )

};
export default Item;